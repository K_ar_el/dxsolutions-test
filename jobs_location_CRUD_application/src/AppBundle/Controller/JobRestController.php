<?php  
	namespace AppBundle\Controller;

	
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\Security\Core\Exception\AccessDeniedException;
	use Symfony\Component\HttpKernel\Exception\HttpException;

	use FOS\RestBundle\Controller\FOSRestController;
	use FOS\RestBundle\View\View;

	use AppBundle\Entity\Job;

	class JobRestController extends FOSRestController
	{
	  public function getJobsAction(){

	    $jobs = $this->getDoctrine()->getRepository('AppBundle:Job')->findAll();
	   

	     return $jobs;
	  }

	  public function getJobAction($id){

	  	$em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('AppBundle:Job')->find($id);

	  	return $job;
	  }
	}
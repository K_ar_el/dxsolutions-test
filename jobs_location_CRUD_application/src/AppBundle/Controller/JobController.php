<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Job;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Controller\JobRestController;

class JobController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function jobsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('AppBundle:Job')->findAll();
        
        return $this->render('jobs/index.html.twig', array('jobs' => $job));
    }

    /**
     * @Route("/detail/{id}", name="jobs_list")
     */
    public function detailAction($id)
    {
        //werkt niet 
        //$em = $this->getDoctrine()->getManager();
        //$details = $em->getRepository('AppBundle:Job')->find($id);

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Job');

        $query = $repository->createQueryBuilder('p')
            ->where('p.id =' . $id)
            ->getQuery();

        $job = $query->getResult();

        
        return $this->render('jobs/detail.html.twig', array('details' => $job));
    }

    /**
     * @Route("/create", name="create_job")
     */
    public function createAction(Request $request)
    {
        $job = new Job();
        // $job->setJobName('Dokter');
        // $job->setDescription('blabla blablabla blablabla');
        // $job->setCompanyName('Bazookas');
        // $job->setStreet('Sint-pieterkaai 4');
        // $job->setCity('Brugge');
        // $job->setZipcode('8900');

        $form = $this->createFormBuilder($job)
                    ->add('jobName', TextType::class)
                    ->add('description', TextareaType::class)
                    ->add('companyName', TextType::class)
                    ->add('street', TextType::class)
                    ->add('city', TextType::class)
                    ->add('zipcode', IntegerType::class)
                    ->add('save',  SubmitType::class, array('label' => 'Create new Job'))
                    ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('jobs/create.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/edit/{id}", name="edit_job")
     */
    public function editAction($id, Request $request)
    {
         $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('AppBundle:Job')->find($id);

        //    $em = $this->getDoctrine()
        //     ->getRepository('AppBundle:Job');

        // $query = $repository->createQueryBuilder('p')
        //     ->where('p.id =' . $id)
        //     ->getQuery();

        // $job = $query->getResult();

        $form = $this->createFormBuilder($job)
                    ->add('jobName', TextType::class)
                    ->add('description', TextareaType::class)
                    ->add('companyName', TextType::class)
                    ->add('street', TextType::class)
                    ->add('city', TextType::class)
                    ->add('zipcode', IntegerType::class)
                    ->add('save',  SubmitType::class, array('label' => 'Update Job'))
                    ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        
        return $this->render('jobs/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/delete/{id}", name="delete_job")
     */
    public function deletAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteJob = $em->getRepository('AppBundle:Job')->find($id);

        $em->remove($deleteJob);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }
}